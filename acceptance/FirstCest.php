<?php 

class FirstCest
{

    public function _before(AcceptanceTester $I)
    {
    }

    public function loginPage(AcceptanceTester $I)
    {

 		# Page is redirected to the default page configured in the acceptance.suite
        $I->amOnPage('/');


        # Checking of the LOG In text if its appears in the whole page 
        $I->see('LOG IN');

        # Entering the text coming in from email variable
        $I->fillField(['id' => 'userIdentifier'], email);

        #Clicking the Log In button
        $I->click("//button[@type='submit']");

        #waiting for the element that has a class of h5 and a text Password, if condition was not met wait for 30 seconds
        $I->waitForElementVisible("//*[@class='h5' and text()='Password']", 30);

        # Check for the Text Password in the page
        $I->see('PASSWORD');

        # Click the element password to show the password field
        $I->click("//*[@class='h5' and text()='Password']");

        # Wait for the Password field to show if element doesn't wait for 30 seconds
        $I->waitForElementVisible(['id' => 'password'], 30);

        # Enter the Password configured outside the page
        $I->fillField(['id' => 'password'], password);

        #Click the Log in Button
        $I->click("//button[@type='submit']");

        #Wait for the element checking account but if element is not visible wait for 30 seconds
        $I->waitForElementVisible("//h2[text()='Checking account']", 30);

        # Check the View Accounts to ensure user is in the mainpage
        $I->see('VIEW ACCOUNTS');

        # Check if the User Id is correct (Note: to ensure the page logged in is for the user)
        $I->see('8588798');
    }
}
